def reverser
  yield.split(' ').map { |i| i.reverse }.join(" ")
end

def adder(num = 1)
  yield + num
end

def repeater(repetitions = 1)
  repetitions.times do
    yield
  end
end

def banana
  phrase = yield
  puts phrase
end
